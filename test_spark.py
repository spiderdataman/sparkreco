from pyspark.sql import SparkSession


spark = SparkSession.builder.appName('test-spark').getOrCreate()
test_list = [['Hello', 'world']], [['I', 'am', 'fine']]

df = spark.createDataFrame(test_list)
print("##Contenu du dataframe ##")
print(df.show())
